import sqlite from 'sqlite'
import SQL from 'sql-template-strings';

const test = async () => {

  const db = await sqlite.open('./db.sqlite');
  
  await db.run(`CREATE TABLE paper-collection (color INTEGER PRIMARY KEY AUTOINCREMENT, price TEXT NOT NULL, matte  text NOT NULL UNIQUE);`);

  const stmt = await db.prepare(SQL`INSERT INTO paper-collection (price, matte ) VALUES (?, ?)`);
  let i = 0;
  while(i<10){
    await stmt.run(`person ${i}`,`person${i}@server.com`);
    i++
  }
 
  await stmt.finalize();

   const rows = await db.all("SELECT color AS id, price, matte  FROM paper-collection")
  rows.forEach( ({ id, price, matte  }) => console.log(`[id:${id}] - ${price} - ${matte }`) )
}

export default { test }
